﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ParaApp.Models
{
    public class MyMarkerContext : DbContext
    {
        public MyMarkerContext()
            : base("DefaultConnection")
        { }
        public DbSet<MyMarkers> MyMarkers { get; set; }
    }
    [Table("MyMarker")]
    public class MyMarkers
    {
        [Key]
        public int ID { get; set; }
        public string UserName { get; set; }
        public int MarkerID { get; set; }
    }

}