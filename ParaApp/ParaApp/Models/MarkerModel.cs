﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ParaApp.Models
{
    public class MarkerContext : DbContext
    {
        public MarkerContext()
            : base("DefaultConnection")
        {

        }
        public DbSet<Marker> Marker { get; set; }
    }
    [Table("Marker")]
    public class Marker
    {
        [Key]
        public int ID { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Name { get; set; }
        public string Vicinity { get; set; }
    }
}