﻿using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;
using System.Web.Mvc;

namespace ParaApp.Models
{
    public class PlaceQuery
    {
        public string Query {get;set;}
        public string Latitude {get;set;}
        public string Longitude {get;set;}
    }
    public class ItemsAndQuery
    {
        public PlaceQuery PlaceQuery { get; set; }
        public List<Item> ItemList { get; set; }
        public string Message { get; set; }
    }
    public class Category
    {
        public string id { get; set; }
        public string title { get; set; }
        public string href { get; set; }
        public string type { get; set; }
        public string system { get; set; }
        public string chaosTarsierField { get; set; }
        public string chaosMandrillField { get; set; }
        public string chaosCapuchinField { get; set; }
        public string chaosProboscisField { get; set; }
        public string chaosTamarinField { get; set; }
        public string chaosGibbonField { get; set; }
        public string chaosDoucField { get; set; }
    }

    public class Tag
    {
        public string id { get; set; }
        public string title { get; set; }
        public string group { get; set; }
    }

    public class Item
    {
        public List<double> position { get; set; }
        public int distance { get; set; }
        public string title { get; set; }
        public double averageRating { get; set; }
        public Category category { get; set; }
        public string icon { get; set; }
        public string vicinity { get; set; }
        public List<object> having { get; set; }
        public string type { get; set; }
        public string href { get; set; }
        public string id { get; set; }
        public string chaosNihonzaruField { get; set; }
        public List<Tag> tags { get; set; }
        public string chaosLemurField { get; set; }
        public string chaosMandrillField { get; set; }
        public string chaosGibbonField { get; set; }
    }

    public class Results
    {
        public string next { get; set; }
        public List<Item> items { get; set; }
    }

    public class Address
    {
        public string text { get; set; }
        public string house { get; set; }
        public string street { get; set; }
        public string postalCode { get; set; }
        public string district { get; set; }
        public string city { get; set; }
        public string county { get; set; }
        public string stateCode { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
    }

    public class Location
    {
        public List<double> position { get; set; }
        public Address address { get; set; }
    }

    public class Context
    {
        public Location location { get; set; }
        public string type { get; set; }
        public string href { get; set; }
    }

    public class Search
    {
        public Context context { get; set; }
        public string chaosOrangutanField { get; set; }
    }

    public class Places
    {
        public Results results { get; set; }
        public Search search { get; set; }
        public string chaosGibbonField { get; set; }
    }

}