﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;

namespace ParaApp.Models
{
    public class ItineraryListContext : DbContext
    {
        public ItineraryListContext()
            : base("DefaultConnection")
        {
        }
        public DbSet<ItineraryList> ItineraryLists { get; set; }
        public DbSet<Marker> Marker { get; set; }
        public DbSet<MyMarkers> MyMarkers { get; set; }
        public DbSet<Temporary> Temporary { get; set; }
    }
    [Table("ItineraryList")]
    public class ItineraryList : DbContext
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Creator { get; set; }
        public string Modified { get; set; }
        public int Likes { get; set; }
        public int Venues { get; set; }
        [Display(Name = "Estimated time to spend")]
        public DateTime RouteTime { get; set; }
    }
    [Table("MyItineraries")]
    public class MyItineraries : DbContext
    {
        public int ItineraryID { get; set; }
        public int UserID { get; set; }
        public string Name { get; set; }
    }
    [Table("Marker")]
    public class Marker : DbContext
    {
        public int ID { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Name { get; set; }
        public string Vicinity { get; set; }
    }
    [Table("MyMarker")]
    public class MyMarkers : DbContext
    {
        [Key]
        public int ID { get; set; }
        public string UserName { get; set; }
        public int MarkerID { get; set; }
    }
    [Table("Temporary")]
    public class Temporary : DbContext
    {
        [Key]
        public string UserName { get; set; }
        public string Coordinates { get; set; }
        public string Method { get; set; }
    }
    public class MarkersAndTemp
    {
        public List<Marker> Markers { get; set; }
        public Temporary Temporary { get; set; }
    }
}