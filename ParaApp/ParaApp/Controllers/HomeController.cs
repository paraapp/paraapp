﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using ParaApp.Models;

namespace ParaApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        public ActionResult TopItineraries()
        {
            List<ItineraryList> ItineraryLists = new List<ItineraryList>();
            using (ItineraryListContext db = new ItineraryListContext())
            {
                ItineraryLists = db.ItineraryLists.ToList();
            }
            return View(ItineraryLists);
        }
    }
}