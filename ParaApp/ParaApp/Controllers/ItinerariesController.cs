﻿using ParaApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ParaApp.Controllers
{
    public class ItinerariesController : Controller
    {
        //
        // GET: /Itineraries/


        //
        // GET: /Itineraries/Details/5

        public ActionResult Index()
        {
            List<Marker> Markers = new List<Marker>();
            using (ItineraryListContext db = new ItineraryListContext())
            {
                List<MyMarkers> myMarkers = new List<MyMarkers>();
                myMarkers = db.MyMarkers.Where(m=>m.UserName == User.Identity.Name).ToList();
                foreach (MyMarkers myMarker in myMarkers)
                {
                    try
                    {
                        Markers.Add(db.Marker.Single(m => myMarker.MarkerID == m.ID));
                    }
                    catch { }
                }
            }
            MarkersAndTemp mAndT = new MarkersAndTemp();
            mAndT.Markers = Markers;
            
            return View(mAndT);
        }

        //
        // GET: /Itineraries/Create

        public ActionResult Add(double latitude,double longitude,string caar)
        {
            
            using (ItineraryListContext db = new ItineraryListContext())
            {
                if (db.Temporary.Where(x=>x.UserName == User.Identity.Name) == null)
                {
                    db.Temporary.Add(new Temporary() {UserName=User.Identity.Name,Coordinates = String.Empty,Method=String.Empty});
                    db.SaveChanges();
                }
                Temporary temp = db.Temporary.Single(x => x.UserName == User.Identity.Name);
                if (temp.Coordinates.Length > 0) temp.Coordinates += ":";
                if (temp.Method.Length > 0) temp.Method += ":";
                temp.Coordinates += string.Format("{0},{1}", latitude, longitude);
                temp.Method += "car";
                db.SaveChanges();
            }

            return View();
        }

        //
        // POST: /Itineraries/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Itineraries/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Itineraries/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Itineraries/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Itineraries/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
