﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ParaApp.Models;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace ParaApp.Controllers
{
    public class PlacesController : Controller
    {
        //
        // GET: /Places/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddMarker(string lat, string lon, string vicinity,string title)
        {
            double latitude = double.Parse(lat);
            double longitude = double.Parse(lon);
            int markerid = new int();

            string result = "Successfully added to My Markers";
            using (ItineraryListContext db = new ItineraryListContext())
            {
                lock (db.Marker)
                {
                    List<Marker> markers = db.Marker.ToList();
                    if (!markers.Exists(m=>m.Latitude==latitude && m.Longitude == longitude && m.Vicinity == vicinity && m.Name == title))
                    {
                        db.Marker.Add(new Marker() { Latitude = latitude, Longitude = longitude, Vicinity = vicinity, Name = title });
                        db.SaveChanges();
                        markerid = db.Marker.Single(m=>m.Latitude==latitude && m.Longitude == longitude && m.Vicinity == vicinity && m.Name == title).ID;
                    }
                }
                lock (db.MyMarkers)
                {
                    List<MyMarkers> mymarkers = db.MyMarkers.ToList();
                    
                    if (!mymarkers.Exists(m=>m.UserName ==User.Identity.Name && m.MarkerID == markerid))
                    {
                        db.MyMarkers.Add(new MyMarkers() { UserName = User.Identity.Name, MarkerID = markerid });
                        db.SaveChanges();
                    }
                    else
                    {
                        result = "Marker already in My Markers";
                    }
                }
            }
            PlaceQuery pq = new PlaceQuery();
            string q = string.IsNullOrWhiteSpace(pq.Query) ? "restaurants" : pq.Query;
            string lat2 = string.IsNullOrWhiteSpace(pq.Latitude) ? "14.5500" : pq.Latitude;
            string lon2= string.IsNullOrWhiteSpace(pq.Longitude) ? "121.0333" : pq.Longitude;
            pq.Query = q;
            pq.Latitude = lat2;
            pq.Longitude = lon2;
            Places places;
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(string.Format("http://places.demo.api.here.com/places/v1/discover/search?at={1}%2C{2}&q={0}&accept=application%2Fjson&app_id={3}&app_code={4}", q, lat, lon, "DemoAppId01082013GAL", "AJKnXv84fjrb0KIHawS0Tg"));
            using (Stream stream = request.GetResponse().GetResponseStream())
            using (StreamReader streamReader = new StreamReader(stream))
            {
                string json = streamReader.ReadToEnd();
                places = JsonConvert.DeserializeObject<Places>(json);
            }
            return View("Index", new ItemsAndQuery() { PlaceQuery = pq, ItemList = places.results.items.ToList(), Message = result });
        }
        //
        // GET: /Places/Details/5
        public ActionResult Places(PlaceQuery pq)
        {
            string q = string.IsNullOrWhiteSpace(pq.Query) ? "restaurants" : pq.Query ;
            string lat = string.IsNullOrWhiteSpace(pq.Latitude) ? "14.5500" : pq.Latitude;
            string lon = string.IsNullOrWhiteSpace(pq.Longitude) ? "121.0333" : pq.Longitude;
            pq.Query = q;
            pq.Latitude = lat;
            pq.Longitude = lon;
            Places places;
            HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(string.Format("http://places.demo.api.here.com/places/v1/discover/search?at={1}%2C{2}&q={0}&accept=application%2Fjson&app_id={3}&app_code={4}", q, lat, lon, "DemoAppId01082013GAL", "AJKnXv84fjrb0KIHawS0Tg"));
            using (Stream stream = request.GetResponse().GetResponseStream())
            using (StreamReader streamReader = new StreamReader(stream))
            {
                string json = streamReader.ReadToEnd();
                places = JsonConvert.DeserializeObject<Places>(json);
            }
            return View("Index",new ItemsAndQuery(){PlaceQuery= pq, ItemList= places.results.items.ToList()});
        }
        //public ActionResult Places()
        //{
        //    string q = "restaurant";
        //    string lat = "14.5500";
        //    string lon = "121.0333";
        //    Places places;
        //    HttpWebRequest request = (HttpWebRequest)System.Net.WebRequest.Create(string.Format("http://places.demo.api.here.com/places/v1/discover/search?at={1}%2C{2}&q={0}&accept=application%2Fjson&app_id={3}&app_code={4}", q, lat, lon, "DemoAppId01082013GAL", "AJKnXv84fjrb0KIHawS0Tg"));
        //    using (Stream stream = request.GetResponse().GetResponseStream())
        //    using (StreamReader streamReader = new StreamReader(stream))
        //    {
        //        string json = streamReader.ReadToEnd();
        //        places = JsonConvert.DeserializeObject<Places>(json);
        //    }
        //    return View("Index",places.results.items.ToList());
        //}
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Places/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Places/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Places/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Places/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Places/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Places/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
