﻿window.paraappApp.markerViewModel = (function (ko, datacontext) {
    /// <field name="todoLists" value="[new datacontext.todoList()]"></field>
    var markers = ko.observableArray(),
        error = ko.observable(),
        addMarker = function () {
            var marker = datacontext.createMarker();
            marker.isEditingListTitle(true);
            datacontext.saveNewMarker(marker)
                .then(addSucceeded)
                .fail(addFailed);

            function addSucceeded() {
                showTodoList(marker);
            }
            function addFailed() {
                error("Save of new todoList failed");
            }
        },
        showMarker = function (marker) {
            markers.unshift(marker); // Insert new todoList at the front
        },
        deleteMarker = function (marker) {
            markers.remove(marker);
            datacontext.deleteMarker(marker)
                .fail(deleteFailed);

            function deleteFailed() {
                showMarker(marker); // re-show the restored list
            }
        };

    datacontext.getTodoLists(markers, error); // load todoLists

    return {
        markers: markers,
        error: error,
        addMarker: addMarker,
        deleteMarker: deleteMarker
    };

})(ko, todoApp.datacontext);

// Initiate the Knockout bindings
ko.applyBindings(window.todoApp.todoListViewModel);
